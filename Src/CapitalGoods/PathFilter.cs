﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace CapitalGoods
{
    public class PathFilter
    {
        private readonly string[] _includeFilters;
        private readonly string[] _excludeFilters;

        public PathFilter([NotNull][ItemNotNull] string[] filters)
        {
            if (filters == null) throw new ArgumentNullException(nameof(filters));
            if (filters.Length == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(filters));

            if (filters.Any(string.IsNullOrEmpty))
            {
                throw new ArgumentException($"Filters must be not null or empty.", nameof(filters));
            }

            var invalidFilters = filters.Where(x => string.IsNullOrEmpty(x) || !(x.StartsWith("+:") || x.StartsWith("-:"))).ToArray();

            if (invalidFilters.Any())
            {
                throw new ArgumentException($"Filters must start with: \"+:\" or \"-:\". Incorrect filsters: {string.Join(", ", invalidFilters)}", nameof(filters));
            }

            _includeFilters = filters.Where(x => x.StartsWith("+:")).Select(x => x.Substring(2)).Select(WildCardToRegular).ToArray();
            _excludeFilters = filters.Where(x => x.StartsWith("-:")).Select(x => x.Substring(2)).Select(WildCardToRegular).ToArray();
        }

        public bool Check([NotNull] string path)
        {
            path = Normalize(path);

            return _includeFilters.Any(pattern => Regex.IsMatch(path, pattern)) &&
                   !_excludeFilters.Any(pattern => Regex.IsMatch(path, pattern));
        }

        private static string WildCardToRegular(string path)
        {
            path = Normalize(path);

            return "^" + Regex.Escape(path).Replace("\\?", ".").Replace("\\*", ".*") + "$";
        }

        private static string Normalize(string path)
        {
            return path.Replace('\\', '/');
        }
    }
}