namespace CapitalGoods.Data
{
    public interface IKeyProvider
    {
        string GetKey<T>(string key = null);
    }
    
    public interface ISerializer
    {
        object Serialize<T>(T obj);
        T Deserialize<T>(object data);
    }

    public class KeyProvider : IKeyProvider
    {
        public string GetKey<T>(string key = null)
        {
            var typeName = typeof(T).FullName;
            return key != null ? $"{typeName}_{key}" : typeName;
        }
    }

    public class Serializer : ISerializer
    {
        public object Serialize<T>(T obj)
        {
            return obj;
        }

        public T Deserialize<T>(object data)
        {
            return (T)data;
        }
    }

    public class Storage : IStorage
    {
        private readonly ISerializer _serializer;
        private readonly IStorage _storage;
        private readonly IKeyProvider _keyProvider;

        public Storage(ISerializer serializer, IStorage storage, IKeyProvider keyProvider)
        {
            _serializer = serializer;
            _storage = storage;
            _keyProvider = keyProvider;
        }

        public bool Contains<T>(string key = null)
        {
            return _storage.Contains<object>(ToInternalKey<T>(key));
        }

        public T Get<T>(string key = null)
        {
            var internalData = _storage.Get<object>(ToInternalKey<T>(key));
            return ToExternalObj<T>(internalData);
        }

        public bool TryGet<T>(out T obj, string key = null)
        {
            object internalData;
            if (_storage.TryGet(out internalData, ToInternalKey<T>(key)))
            {
                obj = ToExternalObj<T>(internalData);
                return true;
            }

            obj = default(T);
            return false;
        }

        public void Put<T>(T obj, string key = null)
        {
            _storage.Put(ToInternalData(obj), ToInternalKey<T>(key));
        }

        public void Delete<T>(string key = null)
        {
            _storage.Delete<object>(ToInternalKey<T>(key));
        }

        public void Clean()
        {
            _storage.Clean();
        }

        private string ToInternalKey<T>(string key)
        {
            var externalKey = _keyProvider.GetKey<T>(key);
            var internalKey = _keyProvider.GetKey<object>(externalKey);

            return internalKey;
        }

        private object ToInternalData<T>(T externalObj)
        {
            return _serializer.Serialize(externalObj);
        }

        private T ToExternalObj<T>(object internalData)
        {
            return _serializer.Deserialize<T>(internalData);
        }
    }
}