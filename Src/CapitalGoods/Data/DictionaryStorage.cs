using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace CapitalGoods.Data
{
    public class DictionaryStorage : IStorage
    {
        private readonly Dictionary<string, object> _dictionary = new Dictionary<string, object>();

        public bool Contains<T>([NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            return _dictionary.ContainsKey(key);
        }

        public T Get<T>([NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            return (T)_dictionary[key];
        }

        public bool TryGet<T>(out T obj, [NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            object raw;
            if (_dictionary.TryGetValue(key, out raw))
            {
                obj = (T)raw;
                return true;
            }

            obj = default(T);
            return false;
        }

        public void Put<T>(T obj, [NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            _dictionary[key] = obj;
        }

        public void Delete<T>([NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            _dictionary.Remove(key);
        }

        public void Clean()
        {
            _dictionary.Clear();
        }
    }
}