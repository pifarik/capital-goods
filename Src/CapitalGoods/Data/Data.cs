﻿namespace CapitalGoods.Data
{
    public interface IStorage : IReadableStorage, IWritableStorage { }
    public interface IReadableStorage
    {
        bool Contains<T>(string key = null);
        T Get<T>(string key = null);
        bool TryGet<T>(out T obj, string key = null);
    }
    public interface IWritableStorage
    {
        void Put<T>(T obj, string key = null);
        void Delete<T>(string key = null);
        void Clean();
    }
}
