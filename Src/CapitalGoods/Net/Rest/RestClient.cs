using System.Threading;
using System.Threading.Tasks;

namespace CapitalGoods.Net.Rest
{
    public class RestClient : IRestClient
    {
        private readonly IRestCommunicator _communicator;

        public RestClient(IRestCommunicator communicator)
        {
            _communicator = communicator;
        }

        public Task GetAsync(string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync(RestMethod.Get, requestUri, cancellationToken: cancellationToken);
        }

        public Task PutAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync(RestMethod.Put, requestUri, request, cancellationToken);
        }

        public Task PostAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync(RestMethod.Post, requestUri, request, cancellationToken);
        }

        public Task PatchAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync(RestMethod.Patch, requestUri, request, cancellationToken);
        }

        public Task DeleteAsync(string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync(RestMethod.Delete, requestUri, cancellationToken: cancellationToken);
        }

        public Task<TResponse> GetAsync<TResponse>(string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync<TResponse>(RestMethod.Get, requestUri, cancellationToken: cancellationToken);
        }

        public Task<TResponse> PutAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync<TResponse>(RestMethod.Put, requestUri, request, cancellationToken);
        }

        public Task<TResponse> PostAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync<TResponse>(RestMethod.Post, requestUri, request, cancellationToken);
        }

        public Task<TResponse> PatchAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync<TResponse>(RestMethod.Patch, requestUri, request, cancellationToken);
        }

        public Task<TResponse> DeleteAsync<TResponse>(string requestUri, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _communicator.SendAsync<TResponse>(RestMethod.Delete, requestUri, cancellationToken: cancellationToken);
        }
    }
}