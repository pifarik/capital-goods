using System.Threading;
using System.Threading.Tasks;

namespace CapitalGoods.Net.Rest
{
    public interface IRestCommunicator
    {
        Task<TResponse> SendAsync<TResponse>(RestMethod restMethod, string requestUri, object request = null, CancellationToken cancellationToken = default(CancellationToken));
        Task SendAsync(RestMethod restMethod, string requestUri, object request = null, CancellationToken cancellationToken = default(CancellationToken));
    }
}