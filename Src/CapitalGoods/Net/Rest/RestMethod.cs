namespace CapitalGoods.Net.Rest
{
    public enum RestMethod
    {
        Get,
        Post,
        Put,
        Patch,
        Delete,
    }
}