﻿using System.Threading;
using System.Threading.Tasks;

namespace CapitalGoods.Net.Rest
{
    public interface IRestClient
    {
        Task GetAsync(string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task PutAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task PostAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task PatchAsync(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task DeleteAsync(string requestUri, CancellationToken cancellationToken = default(CancellationToken));

        Task<TResponse> GetAsync<TResponse>(string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task<TResponse> PutAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task<TResponse> PatchAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task<TResponse> PostAsync<TResponse>(object request, string requestUri, CancellationToken cancellationToken = default(CancellationToken));
        Task<TResponse> DeleteAsync<TResponse>(string requestUri, CancellationToken cancellationToken = default(CancellationToken));
    }
}