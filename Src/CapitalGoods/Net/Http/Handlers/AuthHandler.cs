using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace CapitalGoods.Net.Http.Handlers
{
    public class AuthHandler : DelegatingHandler
    {
        private readonly Dictionary<string, string> _headers;

        public AuthHandler(Dictionary<string, string> headers)
            : this(headers, new HttpClientHandler())
        {
        }

        public AuthHandler(Dictionary<string, string> headers, HttpMessageHandler innerHandler) : base(innerHandler)
        {
            _headers = headers;
        }

        private readonly MediaTypeWithQualityHeaderValue _acceptContentType;
        private readonly MediaTypeHeaderValue _contentType;

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            foreach (var header in _headers)
            {
                request.Content.Headers.Add(header.Key, header.Value);
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}