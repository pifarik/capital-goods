using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace CapitalGoods.Net.Http.Handlers
{
    public class MimeTypeHandler : DelegatingHandler
    {
        public MimeTypeHandler(string mimeType)
            : this(mimeType, new HttpClientHandler())
        {
        }

        public MimeTypeHandler(string mimeType, HttpMessageHandler innerHandler) : base(innerHandler)
        {
            _acceptContentType = new MediaTypeWithQualityHeaderValue(mimeType);
            _contentType = new MediaTypeHeaderValue(mimeType);
        }

        private readonly MediaTypeWithQualityHeaderValue _acceptContentType;
        private readonly MediaTypeHeaderValue _contentType;

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Content != null)
            {
                request.Content.Headers.ContentType = _contentType;
            }

            request.Headers.Accept.Add(_acceptContentType);

            return base.SendAsync(request, cancellationToken);
        }
    }
}