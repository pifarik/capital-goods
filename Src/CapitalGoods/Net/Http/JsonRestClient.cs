using System.Net.Http;
using CapitalGoods.Net.Http.Format;
using CapitalGoods.Net.Http.Handlers;

namespace CapitalGoods.Net.Http
{
    public class JsonRestClient : HttpRestClient
    {
        private const string MimeType = "application/json";

        public JsonRestClient()
            : base(new JsonFormatter(), new MimeTypeHandler(MimeType))
        {
        }

        public JsonRestClient(HttpMessageHandler handler)
            : base(new JsonFormatter(), new MimeTypeHandler(MimeType, handler))
        {
        }

        public JsonRestClient(HttpMessageHandler handler, bool disposeHandler)
            : base(new JsonFormatter(), new MimeTypeHandler(MimeType, handler), disposeHandler)
        {
        }

        public JsonRestClient(HttpClient client)
            : base(new JsonFormatter(), client)
        {
        }
    }
}