namespace CapitalGoods.Net.Http.Format
{
    public interface IFormatter
    {
        string SerializeObject(object obj);
        T DeserializeObject<T>(string str);
    }
}