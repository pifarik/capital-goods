using System.Net.Http;
using CapitalGoods.Net.Http.Format;
using CapitalGoods.Net.Http.Handlers;

namespace CapitalGoods.Net.Http
{
    public class XmlRestClient : HttpRestClient
    {
        private const string MimeType = "application/xml";

        public XmlRestClient()
            : base(new XmlFormatter(), new MimeTypeHandler(MimeType))
        {
        }

        public XmlRestClient(HttpMessageHandler handler)
            : base(new XmlFormatter(), new MimeTypeHandler(MimeType, handler))
        {
        }

        public XmlRestClient(HttpMessageHandler handler, bool disposeHandler)
            : base(new XmlFormatter(), new MimeTypeHandler(MimeType, handler), disposeHandler)
        {
        }

        public XmlRestClient(HttpClient client)
            : base(new XmlFormatter(), client)
        {
        }
    }
}