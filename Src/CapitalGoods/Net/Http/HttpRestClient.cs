using System;
using System.Net.Http;
using System.Net.Http.Headers;
using CapitalGoods.Net.Http.Format;
using CapitalGoods.Net.Rest;

namespace CapitalGoods.Net.Http
{
    public class HttpRestClient : RestClient, IDisposable
    {
        private readonly HttpClient _httpClient;

        #region ctors

        public HttpRestClient(IFormatter formatter)
            : this(formatter, new HttpClientHandler())
        {
        }

        public HttpRestClient(IFormatter formatter, HttpMessageHandler handler)
            : this(formatter, handler, true)
        {
        }

        public HttpRestClient(IFormatter formatter, HttpMessageHandler handler, bool disposeHandler)
           : this(formatter, new HttpClient(handler, disposeHandler))
        {
        }

        public HttpRestClient(IFormatter contentConverter, HttpClient client)
           : base(new HttpRestCommunicator(client, contentConverter))
        {
            _httpClient = client;
        }

        #endregion

        #region Properties

        public Uri BaseAddress { get { return _httpClient.BaseAddress; } set { _httpClient.BaseAddress = value; } }

        public HttpRequestHeaders DefaultRequestHeaders => _httpClient.DefaultRequestHeaders;

        public TimeSpan Timeout { get { return _httpClient.Timeout; } set { _httpClient.Timeout = value; } }

        public long MaxResponseContentBufferSize { get { return _httpClient.MaxResponseContentBufferSize; } set { _httpClient.MaxResponseContentBufferSize = value; } }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        #endregion
    }
}