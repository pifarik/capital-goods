using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CapitalGoods.Net.Http.Format;
using CapitalGoods.Net.Rest;

namespace CapitalGoods.Net.Http
{
    public class HttpRestCommunicator : IRestCommunicator
    {
        private readonly HttpClient _httpClient;
        private readonly IFormatter _formatter;

        private readonly Dictionary<RestMethod, HttpMethod> _restToHttpMethods = new Dictionary<RestMethod, HttpMethod>
        {
            {RestMethod.Get, HttpMethod.Get},
            {RestMethod.Post, HttpMethod.Post},
            {RestMethod.Put, HttpMethod.Put},
            {RestMethod.Patch, new HttpMethod("PATCH")},
            {RestMethod.Delete, HttpMethod.Delete},
        };

        public HttpRestCommunicator(HttpClient httpClient, IFormatter formatter)
        {
            _httpClient = httpClient;
            _formatter = formatter;
        }

        public async Task<TResponse> SendAsync<TResponse>(RestMethod restMethod, string requestUri, object request = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpRequestMessage = new HttpRequestMessage(_restToHttpMethods[restMethod], requestUri);
            if (request != null)
            {
                httpRequestMessage.Content = ConvertRequest(request);
            }

            using (var response = await _httpClient.SendAsync(httpRequestMessage, cancellationToken).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
                return await ConvertResponseAsync<TResponse>(response.Content).ConfigureAwait(false);
            }
        }

        public async Task SendAsync(RestMethod restMethod, string requestUri, object request = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpRequestMessage = new HttpRequestMessage(_restToHttpMethods[restMethod], requestUri);
            if (request != null)
            {
                httpRequestMessage.Content = ConvertRequest(request);
            }

            using (var response = await _httpClient.SendAsync(httpRequestMessage, cancellationToken).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public HttpContent ConvertRequest(object request)
        {
            try
            {
                var asString = request as string;
                if (asString != null)
                {
                    return new StringContent(asString);
                }

                var asBytes = request as byte[];
                if (asBytes != null)
                {
                    return new ByteArrayContent(asBytes);
                }

                var asStream = request as Stream;
                if (asStream != null)
                {
                    return new StreamContent(asStream);
                }

                return new StringContent(_formatter.SerializeObject(request));
            }
            catch (Exception exception)
            {
                throw new RequestContentException($"Can not serialize instance of {request.GetType().FullName}.", exception);
            }
        }

        public async Task<TResponse> ConvertResponseAsync<TResponse>(HttpContent content)
        {
            try
            {
                if (typeof(TResponse) == typeof(string))
                {
                    return (TResponse)(object)await content.ReadAsStringAsync().ConfigureAwait(false);
                }
                if (typeof(TResponse) == typeof(byte[]))
                {
                    return (TResponse)(object)await content.ReadAsByteArrayAsync().ConfigureAwait(false);
                }
                if (typeof(TResponse) == typeof(Stream))
                {
                    return (TResponse)(object)await content.ReadAsStreamAsync().ConfigureAwait(false);
                }
            }
            catch (Exception exception)
            {
                throw new ResponseContentException($"Can not read data of type '{typeof(TResponse).FullName}'.", exception);
            }

            var responseString = await content.ReadAsStringAsync().ConfigureAwait(false);

            try
            {
                return _formatter.DeserializeObject<TResponse>(responseString);
            }
            catch (Exception exception)
            {
                throw new ResponseContentException($"Can not deserialize instance of {typeof(TResponse).FullName}:{Environment.NewLine}{responseString}", exception);
            }
        }
    }

    public class ResponseContentException : ContentException
    {
        public ResponseContentException(string message, Exception innerException)
            :base(message, innerException)
        {
        }
    }
    public class RequestContentException : ContentException
    {
        public RequestContentException(string message, Exception innerException)
            :base(message, innerException)
        {
        }
    }
    public class ContentException : Exception
    {
        public ContentException(string message, Exception innerException)
            :base(message, innerException)
        {
        }
    }
}