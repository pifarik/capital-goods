﻿using System;

namespace CapitalGoods.Domain
{
    public static class Distance
    {
        public static double DistanceInMetres(double lat1, double lon1, double lat2, double lon2)
        {
            if (lat1 == lat2 && lon1 == lon2)
            {
                return 0.0;
            }

            var theta = lon1 - lon2;

            var distance = Math.Sin(DegToRad(lat1)) * Math.Sin(DegToRad(lat2)) +
                           Math.Cos(DegToRad(lat1)) * Math.Cos(DegToRad(lat2)) *
                           Math.Cos(DegToRad(theta));

            distance = Math.Acos(distance);
            if (double.IsNaN(distance))
            {
                return 0.0;
            }

            distance = RadToDeg(distance);
            distance = distance * 60.0 * 1.1515 * 1609.344;

            return distance;
        }

        private static double DegToRad(double deg)
        {
            return deg * Math.PI / 180.0;
        }

        private static double RadToDeg(double rad)
        {
            return rad / Math.PI * 180.0;
        }
    }
}