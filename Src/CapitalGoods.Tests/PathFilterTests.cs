﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CapitalGoods.Tests
{
    [TestClass]
    public class PathFilterTests
    {
        [TestMethod]
        public void Ctor_Null_ShouldThrow()
        {
            string[] filters = null;

            Action ctor = () => new PathFilter(filters);

            ctor.ShouldThrow<ArgumentNullException>().WithMessage("Value cannot be null.\r\nParameter name: filters");
        }

        [TestMethod]
        public void Ctor_ItemIsNull_ShouldThrow()
        {
            Action ctor = () => new PathFilter(new string[] { null });

            ctor.ShouldThrow<ArgumentException>().WithMessage("Filters must be not null or empty.\r\nParameter name: filters");
        }

        [TestMethod]
        public void Ctor_ItemIsEmpty_ShouldThrow()
        {
            Action ctor = () => new PathFilter(new string[] { string.Empty });

            ctor.ShouldThrow<ArgumentException>().WithMessage("Filters must be not null or empty.\r\nParameter name: filters");
        }

        [TestMethod]
        public void Ctor_Empty_ShouldThrow()
        {
            var filters = new string[0];

            Action ctor = () => new PathFilter(filters);

            ctor.ShouldThrow<ArgumentException>().WithMessage("Value cannot be an empty collection.\r\nParameter name: filters");
        }

        [TestMethod]
        public void Ctor_InvalidFilters_ShouldThrow()
        {
            var filters = new[] { "+:*", "*", "path" };

            Action ctor = () => new PathFilter(filters);

            ctor.ShouldThrow<ArgumentException>().WithMessage("Filters must start with: \"+:\" or \"-:\". Incorrect filsters: *, path\r\nParameter name: filters");
        }

        [TestMethod]
        public void Include_Any_ShouldBeIncluded()
        {
            var filters = new[] { "+:*" };

            var path = "any/path";

            var pathFilter = new PathFilter(filters);

            pathFilter.Check(path).Should().BeTrue();
        }
        [TestMethod]
        public void Include_OnlySubfolder_ShouldBeIncludedOnlyFromSubfolder()
        {
            var filters = new[] { "+:any/*", "-:any/exclude/*" };

            var pathFilter = new PathFilter(filters);

            pathFilter.Check("any/path").Should().BeTrue();
            pathFilter.Check("any/exclude/path").Should().BeFalse();
            pathFilter.Check("another/path").Should().BeFalse();
        }

        [TestMethod]
        public void Include_WithExcludeFilter_ShouldBeExcluded()
        {
            var filters = new[] { "+:*", "-:any/*" };

            var path = "any/path";

            var pathFilter = new PathFilter(filters);

            pathFilter.Check(path).Should().BeFalse();
        }
    }
}
