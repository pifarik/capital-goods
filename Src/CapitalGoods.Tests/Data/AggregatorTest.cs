﻿using CapitalGoods.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CapitalGoods.Tests.Data
{
    [TestClass]
    public class AggregatorTest
    {
        [TestMethod]
        public void Test()
        {
            var aggregator = new Aggregator();


        }
    }

    public class Aggregator
    {
        private readonly IReadableStorage[] _storages;

        public Aggregator(params IReadableStorage[] storages)
        {
            _storages = storages;
        }

        public IReadableStorage Contains<T>(string key = null)
        {
            foreach (var storage in _storages)
            {
                if (storage.Contains<T>(key))
                {
                    return storage;
                }
            }
            return null;
        }

        public bool TryGet<T>(out IReadableStorage container, out T obj, string key = null)
        {
            T o;
            foreach (var storage in _storages)
            {
                if (storage.TryGet<T>(out o, key))
                {
                    container = storage;
                    obj = o;
                    return true;
                }
            }

            container = null;
            obj = default(T);
            return false;
        }
    }
}