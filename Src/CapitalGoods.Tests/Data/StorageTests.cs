﻿using System;
using System.Collections.Generic;
using CapitalGoods.Data;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CapitalGoods.Tests.Data
{
    [TestClass]
    public class StorageTests
    {
        [TestMethod]
        public void Test()
        {
            var storage = new Storage(new Serializer(), new DictionaryStorage(), new KeyProvider());

            storage.Put(storage);

            Assert.AreSame(storage, storage.Get<Storage>().Get<Storage>());
        }


        [TestMethod]
        public void Dictionary_Contains_Test()
        {
            IStorage storage = new DictionaryStorage();

            Action act = () => storage.Contains<object>();
            act.ShouldThrow<ArgumentNullException>();

            storage.Contains<object>("").Should().BeFalse();
            storage.Put(new object(), "");
            storage.Contains<object>("").Should().BeTrue();

            storage.Contains<object>(" ").Should().BeFalse();
            storage.Put(new object(), " ");
            storage.Contains<object>(" ").Should().BeTrue();
        }

        [TestMethod]
        public void Dictionary_Get_Test()
        {
            Action act;

            IStorage storage = new DictionaryStorage();

            act = () => storage.Get<object>();
            act.ShouldThrow<ArgumentNullException>();

            act = () => storage.Get<object>("");
            act.ShouldThrow<KeyNotFoundException>();
            storage.Put(new object(), "");
            storage.Get<object>(""); // Ok

            act = () => storage.Get<object>(" ");
            act.ShouldThrow<KeyNotFoundException>();
            storage.Put(new object(), " ");
            storage.Get<object>(" "); // Ok
        }
    }
}