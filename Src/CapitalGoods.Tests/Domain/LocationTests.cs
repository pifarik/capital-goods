﻿using CapitalGoods.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CapitalGoods.Tests.Domain
{
    [TestClass]
    public class LocationTests
    {
        [TestMethod]
        public void DistanceInMetres_PositionsAreTheSame_Zero()
        {
            var location = Distance.DistanceInMetres(1d, 2d, 1d, 2d);
            Assert.AreEqual(0.0, location);
        }

        [TestMethod]
        public void DistanceInMetres_PositionsAreIncorrect_Zero()
        {
            var location = Distance.DistanceInMetres(double.MaxValue, double.MaxValue, double.MinValue, double.MinValue);
            Assert.AreEqual(0.0, location, delta: 0.01);
        }

        [TestMethod]
        public void DistanceInMetres_PositionsAreCorrect_Zero()
        {
            const double delta = 0.01;

            Assert.AreEqual(1223.8285, Distance.DistanceInMetres(53.9379494, 27.689529, 53.9280055, 27.681514), delta);
            Assert.AreEqual(7100.4405, Distance.DistanceInMetres(53.935511, 27.691099, 53.908723, 27.592661), delta);
        }
    }
}
